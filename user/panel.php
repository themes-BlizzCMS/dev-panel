<style>
.uk-space-small {
  height: 10px;
}

.uk-space-medium {
  height: 20px;
}

.uk-principal-title {
  font-family: 'Helvetica', sans-serif;
  font-size: 36.5px;
  font-weight: 400;
  line-height: 1.05;
  padding: 0;
  margin: 0;
  margin-left: -.05em;
  color: #fff;
}
</style>   

   <section class="uk-section uk-section-small" data-uk-height-viewport="expand: true">
      <div class="uk-container">
        <div class="uk-grid uk-grid-medium" data-uk-grid>
          <div class="uk-width-1-5@m"></div>
          <div class="uk-width-3-5@m">
            <div class="uk-text-center">
              <?php if($this->wowgeneral->getUserInfoGeneral($this->session->userdata('wow_sess_id'))->num_rows()): ?>
                <img class="uk-border-circle" src="<?= base_url('assets/images/profiles/'.$this->wowauth->getNameAvatar($this->wowauth->getImageProfile($this->session->userdata('wow_sess_id')))); ?>" width="120" height="120" alt="" />
              <?php else: ?>
                <img class="uk-border-circle" src="<?= base_url('assets/images/profiles/default.png'); ?>" width="120" height="120" alt="" />
              <?php endif; ?>
              <div class="uk-space-small"></div>
              <div class="uk-principal-title uk-text-uppercase"><?= $this->session->userdata('blizz_sess_username'); ?></div>
              <span class="uk-label"><?= $this->lang->line('panel_last_ip'); ?>: <?= $this->user_model->getLastIp($this->session->userdata('wow_sess_id')); ?></span>
            </div>
			<div class="uk-space-medium"></div>

<div class="uk-scrollspy-inview uk-animation-slide-bottom" uk-scrollspy-class="">
              <div class="uk-column-1-1 uk-column-1-2@s uk-column-divider">
                <div>
                  <p><i class="far fa-user-circle"></i> <?= $this->lang->line('panel_acc_rank'); ?>: <span class="uk-label uk-label-danger">
                    <?php if($this->wowauth->getIsAdmin($this->session->userdata('wow_sess_gmlevel'))): ?>Administrator</span>
					<?php else: ?>
					<span class="uk-label uk-label-success"><?php if($this->wowauth->getIsModerator($this->session->userdata('wow_sess_gmlevel'))): ?>Moderator</span>
					<?php else: ?>
					<span class="uk-label">Player</span>
					<?php endif; ?>
					<?php endif; ?>
                  </p>
                  <p><i class="fas fa-globe"></i> <?= $this->lang->line('panel_location'); ?>: <span class="uk-label">Azeroth</span></p>
                  <p><i class="fas fa-gamepad"></i> Expansion: <span class="uk-label"><?= $this->wowgeneral->getExpansionName(); ?></span></p>
                </div>
                <div>
                  <p><i class="far fa-credit-card"></i> <?= $this->lang->line('panel_dp'); ?>: <span class="uk-badge"><?= $this->wowgeneral->getCharDPTotal($this->session->userdata('wow_sess_id')); ?></span></p>
                  <p><i class="fas fa-star"></i> <?= $this->lang->line('panel_vp'); ?>: <span class="uk-badge"><?= $this->wowgeneral->getCharVPTotal($this->session->userdata('wow_sess_id')); ?></span></p>
                  <p><i class="far fa-clock"></i> Member sinse: <span class="uk-label"><?= $this->wowauth->getJoinDateID($this->session->userdata('wow_sess_id')); ?></span></p>
                </div>
              </div>
              <hr class="uk-divider-icon">
              <div class="uk-grid uk-grid-small uk-child-width-1-1 uk-child-width-1-2@s" data-uk-grid>
                <div>
                   <?php if($this->wowmodule->getVoteStatus() == '1'): ?>
                  <a href="<?= base_url('vote'); ?>" class="uk-button uk-button-default uk-width-1-1"><i class="fas fa-star"></i> <?= $this->lang->line('navbar_vote_panel'); ?></a>
                  <?php else: ?>
                  <button class="uk-button uk-button-default uk-width-1-1 uk-disabled"><i class="fas fa-star"></i> <?= $this->lang->line('navbar_vote_panel'); ?></button>
                  <?php endif; ?>
                </div>
                <div>
                  <?php if($this->wowmodule->getDonationStatus() == '1'): ?>
                  <a href="<?= base_url('donate'); ?>" class="uk-button uk-button-default uk-width-1-1"><i class="far fa-credit-card"></i> <?= $this->lang->line('navbar_donate_panel'); ?></a>
                  <?php else: ?>
                  <button class="uk-button uk-button-default uk-width-1-1 uk-disabled"><i class="far fa-credit-card"></i> <?= $this->lang->line('navbar_donate_panel'); ?></button>
                  <?php endif; ?>
                </div>
				<div>
				  <?php if($this->wowmodule->getStoreStatus() == '1'): ?>
				 <a href="<?= base_url('store'); ?>" class="uk-button uk-button-default uk-width-1-1"><i class="fas fa-store"></i> <?= $this->lang->line('tab_store'); ?></a>
                 <?php else: ?>
				 <button class="uk-button uk-button-default uk-width-1-1 uk-disabled"><i class="fas fa-store"></i> <?= $this->lang->line('tab_store'); ?></button>
				 <?php endif; ?>
				</div>
                <div>
				 <?php if($this->wowmodule->getBugtrackerStatus() == '1'): ?>
				 <a href="<?= base_url('bugtracker'); ?>" class="uk-button uk-button-default uk-width-1-1"><i class="fas fa-bug"></i> <?= $this->lang->line('tab_bugtracker'); ?></a>
                 <?php else: ?>
				 <button class="uk-button uk-button-default uk-width-1-1 uk-disabled"><i class="fas fa-bug"></i> <?= $this->lang->line('tab_bugtracker'); ?></button>
				 <?php endif; ?>
				</div>
				<div>
				 <?php if($this->wowmodule->getChangelogsStatus() == '1'): ?>
				 <a href="<?= base_url('changelogs'); ?>" class="uk-button uk-button-default uk-width-1-1"><i class="fas fa-scroll"></i> <?= $this->lang->line('tab_changelogs'); ?></a>
                 <?php else: ?>
				 <button class="uk-button uk-button-default uk-width-1-1 uk-disabled"><i class="far fa-credit-card"></i> <?= $this->lang->line('tab_changelogs'); ?></button>
				 <?php endif; ?>
				</div>
				<div>
				 <a href="<?= base_url('settings'); ?>" class="uk-button uk-button-default uk-width-1-1"><i class="fas fa-cog"></i> <?= $this->lang->line('button_account_settings'); ?></a>
				</div>
				<div>
				<?php if($this->wowauth->getIsModerator($this->session->userdata('wow_sess_gmlevel'))): ?>
				 <a href="<?= base_url('mod'); ?>" target="_blank" class="uk-button uk-button-default uk-width-1-1"><i class="fas fa-gavel"></i> <?= $this->lang->line('button_mod_panel'); ?></a>
                 <?php else: ?>
				 <button class="uk-button uk-button-default uk-width-1-1 uk-disabled"><i class="fas fa-gavel"></i> <?= $this->lang->line('button_mod_panel'); ?></button>
				 <?php endif; ?>
				</div>
				<div>
				<?php if($this->wowmodule->getACPStatus() == '1'): ?>
				<?php if($this->wowauth->getIsAdmin($this->session->userdata('wow_sess_gmlevel'))): ?>
				 <a href="<?= base_url('admin'); ?>"  target="_blank" class="uk-button uk-button-default uk-width-1-1"><i class="fas fa-cog"></i> <?= $this->lang->line('button_admin_panel'); ?></a>
                 <?php else: ?>
				 <button class="uk-button uk-button-default uk-width-1-1 uk-disabled"><i class="fas fa-cog"></i> <?= $this->lang->line('button_admin_panel'); ?></button>
				 <?php endif; ?>
				 <?php endif; ?>
				</div>
               <div class="uk-space-medium"></div>
              <div class="uk-overflow-auto uk-width-1-1 uk-margin-small" data-uk-grid>
                <?php foreach ($this->wowrealm->getRealms()->result() as $charsMultiRealm):
                    $multiRealm = $this->wowrealm->realmConnection($charsMultiRealm->username, $charsMultiRealm->password, $charsMultiRealm->hostname, $charsMultiRealm->char_database);
                  ?>
                <div>
				
                  <h3 class="uk-h3 uk-heading-line"><span><i class="fas fa-server"></i> <?= $this->wowrealm->getRealmName($charsMultiRealm->realmID); ?> - <?= $this->lang->line('panel_chars_list'); ?></span></h3>
                  <div class="uk-overflow-auto uk-width-1-1 uk-margin-small" data-uk-grid>
                    <?php foreach($this->wowrealm->getGeneralCharactersSpecifyAcc($multiRealm , $this->session->userdata('wow_sess_id'))->result() as $chars): ?>
                    <div>
                      <a href="<?= base_url(''); ?>">
                        <img class="uk-border-circle" src="<?= base_url('assets/images/class/'.$this->wowgeneral->getClassIcon($chars->class)); ?>" title="<?= $chars->name ?> (Lvl <?= $chars->level ?>)" width="50" height="50" uk-tooltip>
                      </a>
                    </div>
                    <?php endforeach; ?>
                  </div>
                </div>
                <?php endforeach; ?>
              </div>
            </div>
			</div>
          <div class="uk-width-1-5@m"></div>
        </div>
      </div>
    </section>